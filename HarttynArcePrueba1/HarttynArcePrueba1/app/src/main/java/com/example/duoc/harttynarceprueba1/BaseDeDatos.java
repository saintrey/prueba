package com.example.duoc.harttynarceprueba1;

import java.util.ArrayList;

/**
 * Created by harce on 20-04-2016.
 */
public class BaseDeDatos {
    private static ArrayList<Usuario> values = new ArrayList<>();
    public static void agregarUsuario(Usuario usuario){
        values.add(usuario);
    }

    public static ArrayList<Usuario> obtieneListadoUsuarios(){
        return values;
    }
}
