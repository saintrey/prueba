package com.example.duoc.harttynarceprueba1;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends AppCompatActivity {

    private Button btnEntrar, btnRegistro;
    private EditText txtUsuario, txtClave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnRegistro = (Button) findViewById(R.id.btnRegistro);

        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtClave = (EditText) findViewById(R.id.txtClave);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarUsuario();
            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirRegistro();
            }
        });

        for(int x = 0 ; x < 10; x++){
            Usuario aux = new Usuario();
            aux.setNombre("" + x);
            aux.setClave("" + x);
            BaseDeDatos.agregarUsuario(aux);
        }
    }

    private void abrirRegistro() {
        Intent i = new Intent(this, RegistrarActivity.class);
        startActivity(i);
    }

    private void validarUsuario() {

        if (txtUsuario.getText().toString().length() < 1 || txtClave.getText().toString().length() < 1) {
            Toast.makeText(this, "Debe ingresar usuario y clave", Toast.LENGTH_LONG).show();
        } else {
            boolean existeUsuario = false;
            for (Usuario aux : BaseDeDatos.obtieneListadoUsuarios()) {
                if (aux.getNombre().equals(txtUsuario.getText().toString()) &&
                        aux.getClave().equals(txtClave.getText().toString())) {
                    existeUsuario = true;
                    break;
                }
            }

            if (existeUsuario) {
                Intent i = new Intent(this, ListadoUsuariosActivity.class);
                i.putExtra(ListadoUsuariosActivity.KEY_PARAM, txtUsuario.getText().toString());
                startActivity(i);
            } else {
                Toast.makeText(this, "Usuario no existe", Toast.LENGTH_LONG).show();
            }
        }
    }

}
