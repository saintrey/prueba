package com.example.duoc.harttynarceprueba1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class RegistrarActivity extends AppCompatActivity {

    private Button btnCrearUsuario, btnVolver;
    private EditText txtUsuario, txtClave, txtReClave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        btnCrearUsuario = (Button) findViewById(R.id.btnCrearUsuario);
        btnVolver = (Button) findViewById(R.id.btnVolver);

        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtClave = (EditText) findViewById(R.id.txtClave);
        txtReClave = (EditText) findViewById(R.id.txtReClave);

        btnCrearUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearUsuario();
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void crearUsuario() {
        if(txtClave.getText().toString().equals(txtReClave.getText().toString()) && txtReClave.getText().toString().length() > 0){
            Usuario usuario = new Usuario();
            usuario.setClave(txtClave.getText().toString());
            usuario.setNombre(txtUsuario.getText().toString());
            BaseDeDatos.agregarUsuario(usuario);
            txtClave.setText("");
            txtReClave.setText("");
            txtUsuario.setText("");

            Toast.makeText(this, "Usuario creado correctamente", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, "Las contraseņas deben ser iguales y deben contener al menos un caracter", Toast.LENGTH_LONG).show();
        }
    }


}
