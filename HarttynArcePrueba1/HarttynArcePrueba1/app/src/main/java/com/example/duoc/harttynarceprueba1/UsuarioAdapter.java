package com.example.duoc.harttynarceprueba1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Duoc on 21-04-2016.
 */
public class UsuarioAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Usuario> dataSet;

    public UsuarioAdapter(Context context, ArrayList<Usuario> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public Object getItem(int position) {
        return dataSet.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.usuarios_item, parent, false);
        }

        // Set data into the view.
        TextView txtUsuario = (TextView) rowView.findViewById(R.id.txtUsuario);
        TextView txtClave = (TextView) rowView.findViewById(R.id.txtClave);

        final Usuario item = this.dataSet.get(position);
        txtUsuario.setText(item.getNombre());
        txtClave.setText(item.getClave());

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //eventoItem(item);
                Toast.makeText(context, "Usuario " + item.getNombre() + " - Clave " + item.getClave(), Toast.LENGTH_LONG).show();
            }
        });

        txtUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventoUsuario();
            }
        });

        txtClave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventoClave();
            }
        });

        return rowView;
    }

    private void eventoClave() {
        Toast.makeText(context, "Este es el on Click de clave", Toast.LENGTH_LONG).show();
    }

    private void eventoUsuario() {
        Toast.makeText(context, "Este es el on Click de Usuario", Toast.LENGTH_LONG).show();
    }

   /* private void eventoItem(Usuario usuario) {
        Toast.makeText(context, "Usuario " + usuario.getNombre() + " - Clave " + usuario.getClave(), Toast.LENGTH_LONG).show();
    }*/
}
