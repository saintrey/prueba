package com.example.duoc.harttynarceprueba1;

/**
 * Created by harce on 20-04-2016.
 */
public class Usuario {
    private String nombre;
    private String clave;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
