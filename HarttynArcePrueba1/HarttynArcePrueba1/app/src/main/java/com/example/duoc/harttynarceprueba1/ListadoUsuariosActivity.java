package com.example.duoc.harttynarceprueba1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


public class ListadoUsuariosActivity extends ActionBarActivity {

    public static final String KEY_PARAM = "usuario";

    private ListView listView;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuarios);

        String parametro = getIntent().getStringExtra(KEY_PARAM);
        listView = (ListView)findViewById(R.id.listView);
        btnSalir = (Button)findViewById(R.id.btnSalir);

        UsuarioAdapter adapter = new UsuarioAdapter(this, BaseDeDatos.obtieneListadoUsuarios());
        listView.setAdapter(adapter);

        Toast.makeText(this, "El usuario logueado es: " + parametro, Toast.LENGTH_LONG).show();

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


}
